import { async } from "q";
import Axios from "axios";

/**
 * 通过 JavaScript 的形式模拟静态接口
 * 这样做的目的是可以直接打包出来部署在线上而不依赖后端或者 mock 接口
 * 
 * me：这里定义了在action中使用的post方法
 * 所有方法都定义了exprot对外提供使用
 * 
 * 如果要请求服务端，需要将这里mock的方法改为真实的post请求。
 * 服务端需要将服务提前部署好。
 */
export async function getUserProfile() {  // 使用async/await方式进行ajax异步请求
  const data = await {
    name: '淘小宝',
    department: '技术部',
    avatar: 'https://img.alicdn.com/tfs/TB1L6tBXQyWBuNjy0FpXXassXXa-80-80.png',
    userid: 10001,
  };
  return { data };
}

/**
 * 用户登陆mock方法
 * @param {} params 
 */
export async function login(params) {
  const { password, username } = params;
  let data = {};
  if (username === 'admin' && password === 'admin') {
    data = await {
      status: 200,
      statusText: 'ok',
      currentAuthority: 'admin',
    };
  } else if (username === 'user' && password === 'user') {
    data = await {
      status: 200,
      statusText: 'ok',
      currentAuthority: 'user',
    };
  } else {
    data = await {
      status: 401,
      statusText: 'unauthorized',
      currentAuthority: 'guest',
    };
  }

  return { data };
}

/**
 * post方式注册用户（mock方法）
 * email:"yitian.z@foxmail.com"
 * name:"yitian"
 * passwd:"yitian123456"
 * rePasswd:"yitian123456"
 */
export async function postUserRegister(params) {
  console.log(params);
  const data = await {
    status: 200,
    statusText: 'ok',
    currentAuthority: 'user',
  };
  return { data };
}

// me: 真正的服务端请求注册（服务端返回注册的用户信息）
export async function postUserRegisterReal(params) {
  const data = await Axios.get('http://localhost:8080/user/register', {
    // headers: {
    //   'Access-Control-Allow-Origin': 'http://localhost:4444',
    //   'Access-Control-Allow-Credentials': 'true',
    //   'Access-Control-Allow-Headers': 'Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With',
    // },
    params: { // 设置请求路径
      name: params.name,
      email: params.email,
      passwd: params.passwd,
      rePasswd: params.rePasswd,
    }
  });
  console.log(data);
  return {data};
}

export async function postUserLogout() {
  const data = await {
    status: 200,
    statusText: 'ok',
    currentAuthority: 'guest',
  };
  return { data };
}
