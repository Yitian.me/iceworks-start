import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Container from '@icedesign/container';
import FeatureItem from './FeatureItem'; // 当前路径下定义的表格component
import styles from  './index.module.scss'; // 主页面的样式
/**
 * 封装的名为index的组件，在定义元素的时候使用const element =  <Index />的方式使用
 * 1. 可以使用函数定义了一个组件：
 * function HelloMessage(props) {
 *     retuen <h1>Hello world!</h1>
 * }
 * 2. 使用es6 class来定义一个组件：
 * class Welcome extends React.Component {
 *     render() {
 *          return <h1>Hello world!</h1> 
 *     }
 * }
 * const element=<HelloMessage /> 为用户自定义的组件：
 * 注意，原生 HTML 元素名以小写字母开头，而自定义的 React 类名以大写字母开头，比如 HelloMessage 不能写成 helloMessage。
 * 除此之外还需要注意组件类只能包含一个顶层标签，否则也会报错。
 */
export default class Index extends Component {
  static displayName = 'Index'; // 显示的名称

  // 验证属性值的类型（string）
  static propTypes = { 
    value: PropTypes.string,
  };

  // 设置默认的props值
  static defaultProps = {
    value: 'string data',
  };

  /**
   * me：构造方法
   * 下面演示了如何在应用中组合使用state和props。
   * 可以在父组件中设置state，并通过在子组件中使用props将其传递到子组件中。
   * 在render方法中，设置属性的名称来获取父组件中传递过来的数据
   */ 
  constructor(props) {
    super(props);   
    this.state = {  // 定义对象
      dataSource: [ // 定义数组
        {           // 对象数组中的对象
          icon: require('./images/fly.png'),
          title: '微淘推送',
          desc:
            '微淘是达人的粉丝回访主阵地。推送到微淘的内容，可以通过微淘内容流展现给粉丝，粉丝喜爱的内容将获得平台推荐',
          status: '10条/天',
          detail:
            '各等级可推送条数不同，L0、L1达人每日可发布1条，L2及以上达人每日可发布10条（具体规则详见达人成长-等级权益）；注：账号升级后，约一至两天内微淘条数生效',
          url: '#',
        },
        {
          icon: require('./images/v.png'),
          title: '阿里V任务',
          desc:
            '阿里V任务是阿里官方内容推广平台。无缝连接达人与商家，达人可以通过营销推广任务与商家进行合作，实现内容变现',
          status: '已开通',
          detail:
            '阿里V任务是阿里官方内容推广平台。无缝连接达人与商家，达人可以通过营销推广任务与商家进行合作，实现内容变现',
          url: '#',
        },
        {
          icon: require('./images/cps.png'),
          title: 'CPS结算权限',
          desc:
            'CPS为按成交付费的商业化模式。获得CPS结算权限的达人，其内容中推荐商品引导成交，即可获得商家设置的淘宝客佣金收入；注：账号升级后，约一至两天内结算权限生效',
          status: '已开通',
          detail: '面对L2及以上等级达人，以及有部分频道权限的达人开放',
          url: '#',
        },
        {
          icon: require('./images/post.png'),
          title: '内容号推送',
          desc:
            '内容号是达人专属的粉丝强回访阵地。通过内容号推送的内容，可以精准推送给粉丝，粉丝在手机淘宝-消息-内容号中查看消息',
          status: '10条/天',
          detail: '面对L4及以上的非店铺达人开放',
          url: '#',
        },
      ],
    };
  }

  // 下面方法将上述定义的dataSource传入FeatureItem（定义好的页面布局中）
  render() {
    return (
      <Container>
        <div className={styles.header}> {/* 定义style */}
          <h2 className={styles.function} >功能状态</h2>
        </div>
        <div>
          {/** 
          me：这里是一个map集合操作 
          可以使用js中map方法来创建列表
          在创建列表时，必须指明列表中的key，当元素没有确定的id时，可以使用它的序列号索引index作为key
          如下即使用的这种方式
          */}
          {this.state.dataSource.map((item, index) => {
            return <FeatureItem data={item} key={index} />;
          })}
        </div>
      </Container>
    );
  }
}