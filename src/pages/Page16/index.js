import React, { Component } from 'react';
import AccountFeatures from './components/AccountFeatures';

export default class Page16 extends Component {
  /**
   * me：构造方法
   * state和props主要的区别在于props是不可变的，而state可以根据与用户的交互来改变。
   */ 
  constructor(props) {
    // react把组件看成一个状态机，通过与用户的交互，实现不同状态然后渲染ui，
    // 让用户界面和数据保持一致
    // react中，只需要更新组件的state，然后根据新的state重新渲染用户界面（不要操作dom）
    super(props);
    this.state = {};
  }
  // me？
  // 下面<div>中的<AccountFeatures>都是在其他地方定义出来的class组件
  render() {
    return (
      <div className="page16-page">
        {/* 罗列账户功能列表 */}
        <AccountFeatures />
      </div>
    );
  }
}
