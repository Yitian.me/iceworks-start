import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { createHashHistory } from 'history';

// 引入默认全局样式
import '@alifd/next/reset.scss';

// 引入基础配置文件
import router from './router';
import configureStore from './configureStore';
import LanguageProvider from './components/LocaleProvider';
import { getLocale } from './utils/locale';

// me：定义react的元素值（元素是构成react应用的最小单位，用于描述屏幕上输出的内容）
const initialState = {};
const history = createHashHistory(); // 外部引用方法
// 获取store（redux）
const store = configureStore(initialState, history); // 外部引用方法
const locale = getLocale(); // utils中的方法，获取当前语言
// ice-container元素定义在index.html文件中，这里对dom文件中对元素进行获取，然后对其中对内容进行渲染
// 在此div中对所有内容都由react dom来管理
const ICE_CONTAINER = document.getElementById('ice-container');
// 如果获取该元素失败，抛出异常
if (!ICE_CONTAINER) {
  throw new Error('当前页面不存在 <div id="ice-container"></div> 节点.');
}

// 需要将react其他元素渲染到dom节点中，需要通过如下方法将其渲染到页面中
// 下面，locale，store，history都是上面已经定义到元素
// router方法定义在router.jsx文件中，用于对具有不同layout布局的页面进行路由

/** 
 * provider是redux中的一个容器组件，用来接受store，并且让store对子组件可用
 * <Provider store={store}>
 *  <App />
 * </Provider>
 * 这样，<Provider>中对子组件<App />才能使用connect方法关联store。
 */
ReactDOM.render(
  <LanguageProvider locale={locale}>
    <Provider store={store}>
      <ConnectedRouter history={history}>{router()}</ConnectedRouter>
    </Provider>
  </LanguageProvider>,
  ICE_CONTAINER
);

// jsx的理解
// react使用jsx来替代常规的js文件
// 元素是构成react应用的最小元素，jsx就是用来声明元素的，要将reasct元素渲染到根dom节点中，需要将其传递给ReactDOM.render()方法
// 将其渲染到页面中。

// 注意：由于 JSX 就是 JavaScript，一些标识符像 class 和 for 不建议作为 XML 属性名。
// 作为替代，React DOM 使用 className 和 htmlFor 来做对应的属性。